# Import serial library to manipulate the serial port
import serial
# Import sys library to write the log file
import sys

# Define collect_serial_data function, with the predefined arguments for baudrate 115200, bytesize 8, no parity, and 1 stop bit
def collect_serial_data(serial_port, file_path, baudrate=115200, bytesize=8, parity='N', stopbits=1):
    # Clear ser variable
    ser = None
    # Begin reading from serial port
    try:
        # Open the serial port
        ser = serial.Serial(serial_port, baudrate=baudrate, bytesize=bytesize, parity=parity, stopbits=stopbits)

        # Open or create the file for collecting logs
        with open(file_path, 'w') as log_file:
            # Print message we are starting the collection
            print(f"Collecting data from {serial_port}. Press Ctrl+C to stop.")

            # Continuously read data from the serial port and write it to the file
            while True:
                # Set data variable to the current line that's being read
                data = ser.readline().decode('utf-8')
                # Write current line to log file
                log_file.write(data)
                # Print current line to console
                print(data, end='')  # Optional: print data to console

    # Exit if someone stops the script
    except KeyboardInterrupt:
        # Print message to let the user know why the script stopped
        print("\nData collection stopped by user.")
    # Exit if an error occurred during serial port reading
    except Exception as e:
        # Print message to show error message to the user
        print(f"An error occurred: {str(e)}")
    # Cleanly close the port and file when done
    finally:
        if ser and ser.is_open:
            # Close the serial port
            ser.close()
        if 'log_file' in locals() and not log_file.closed:
            # Close the file
            log_file.close()

# Main function call
if __name__ == "__main__":
    # Show usage if not enough arguments are supplied
    if len(sys.argv) != 3:
        print("Usage: python script.py <serial_port> <file_path>")
        sys.exit(1)

    # Get serial port and file path from command-line arguments
    serial_port = sys.argv[1]
    file_path = sys.argv[2]

    # Call the function to start collecting data
    collect_serial_data(serial_port, file_path)