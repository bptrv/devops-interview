terraform {
  required_providers {
    aws = {                     # This is our provider (AWS)
      source  = "hashicorp/aws" # The source
      version = "4.67.0"        # Following up with the version of it
    }
  }
  required_version = ">= 1.7.0"
}

# Set the region. Find it out using "aws ec2 describe-regions"
provider "aws" {
  region = "eu-central-1"
}
