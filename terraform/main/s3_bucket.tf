############# S3 Bucket ################
resource "aws_s3_bucket" "aws-test-bucket" {      # This is the resource of the bucket
  bucket        = "aws-test-bucket-pcd-interview" # The name of the bucket
  force_destroy = true                            # Should be deleted from the bucket when the bucket is destroyed so that the bucket can be destroyed without error

  tags = {
    Name        = "Test bucket"
    Environment = "Dev"
  }
}