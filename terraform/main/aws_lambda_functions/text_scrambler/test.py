import json #модул с име json - значи ще обработваме json стрингове
import random # модул с име random - значи ще рандомизираме някакви неща(стрингове?)

# def text("аз искам бира", str)

def scramble(text: str) -> str: # дефинираме функция с име scramble, която да взема като инпут text variable-a, 

    return "".join(random.sample(text, len(text))) # return = output.

def handler(event, context):
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": json.dumps({"result": scramble(event["text"])}),
    }

if __name__ == "__main__":
    pass
