################ IAM ######################
resource "aws_iam_role" "my_apprunner_role" {
  name = "my-apprunner-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "tasks.apprunner.amazonaws.com",
        },
      },
    ],
  })
}

############# Communication with S3 ##############
resource "aws_iam_role_policy" "my_apprunner_role_policy" {
  name = "my-apprunner-role-policy"
  role = aws_iam_role.my_apprunner_role.id

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:ListBucket",
        ],
        Effect = "Allow",
        Resource = [
          "${aws_s3_bucket.aws-test-bucket.arn}/*",
          aws_s3_bucket.aws-test-bucket.arn,
        ],
      },
    ],
  })
}

################ Resource with Image Repository Source######################
resource "aws_apprunner_service" "test-runner" {
  service_name = "aws-test-runner-pcd-interview"
  instance_configuration {
    instance_role_arn = aws_iam_role.my_apprunner_role.arn
  }

  source_configuration {
    image_repository {
      image_configuration {
        port = "8000"
      }
      image_identifier      = "public.ecr.aws/aws-containers/hello-app-runner:latest"
      image_repository_type = "ECR_PUBLIC"
    }
    auto_deployments_enabled = false
  }

  network_configuration {
    egress_configuration {
      egress_type       = "VPC"
      vpc_connector_arn = aws_apprunner_vpc_connector.connector.arn
    }
  }

  tags = {
    Name = "aws-test-runner-pcd-interview"
  }
}