################# VPC and Subnets #################

# Creation of a new VPC for this project.
resource "aws_vpc" "project_vpc" { # Our VPC resource
  cidr_block = "10.0.0.0/16"
}

# New subnet
resource "aws_subnet" "db_subnet_1" {
  vpc_id                  = aws_vpc.project_vpc.id # Creation of the first subnet
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "eu-central-1a"
  map_public_ip_on_launch = false # That option indicates whether instances launched in this subnet receive a public IP address (In my case is set to FALSE, as we do not need a public access to out DB)
}

# We need two subnets because of Amazon's availability zones
resource "aws_subnet" "db_subnet_2" { # This is the second subnet
  vpc_id                  = aws_vpc.project_vpc.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "eu-central-1b"
  map_public_ip_on_launch = false # That option indicates whether instances launched in this subnet receive a public IP address (In my case is set to FALSE, as we do not need a public access to out DB)
}

resource "aws_db_subnet_group" "db_subnet_group" {                              # The resource  db_subnet_group
  name       = "pcd_db-group"                                                   # Project Cloud DevOps db-group
  subnet_ids = ["${aws_subnet.db_subnet_1.id}", "${aws_subnet.db_subnet_2.id}"] # here is the list of subnet ID's
}
################# SECURITY GROUPS ############
################# PostgreSQL #################

resource "aws_security_group" "rds" { # This is security group for RDS
  vpc_id = aws_vpc.project_vpc.id     # ID of RDS vpc

  ingress { # Ingress for PostgreSQL to communicate with lambda via port 5432
    description     = "PostgreSQL"
    from_port       = 5432                           # The port "from" is 5432
    protocol        = "tcp"                          # Communication protocol is TCP
    to_port         = 5432                           # The port "to" is 5432
    security_groups = [aws_security_group.lambda.id] # Security group for lambda
  }
}

################# lambda #################

resource "aws_security_group" "lambda" { # Resource security group for lambda platform
  vpc_id = aws_vpc.project_vpc.id        # Described id of the VPC

  egress {              # Here is the egress reaching the CIDR blocks bellow
    from_port   = 5432  # The port "from" is 5432
    to_port     = 5432  # The port "to" is 5432
    protocol    = "tcp" # Communication protocol is TCP
    cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24"]
  }
}

################ App_Runner #################

resource "aws_security_group" "app_runner" {
  vpc_id = aws_vpc.project_vpc.id

  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["10.0.1.0/24", "10.0.2.0/24"]
  }
}

resource "aws_apprunner_vpc_connector" "connector" {                                           # Connector is necessary to talk to subnet groups
  vpc_connector_name = "apprunner-connector"                                                   # The name of our connector
  subnets            = ["${aws_subnet.db_subnet_1.id}", "${aws_subnet.db_subnet_2.id}"]        # The list of our ID's of subnets for our VPC
  security_groups    = ["${aws_security_group.app_runner.id}", "${aws_security_group.rds.id}"] # List of IDs of security groups that App Runner should use for access to AWS resources under the specified subnets
}
