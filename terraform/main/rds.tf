
# Block for creating the DB
resource "aws_db_instance" "default" {
  allocated_storage      = 20                                       # GiB, minimum value
  storage_type           = "gp2"                                    # ssd general purpose, lowest tier, scales with usage
  engine                 = "postgres"                               # which db engine - mysql oracle postgre etc
  engine_version         = "16.1"                                   # db version, checked latest with "aws rds describe-db-engine-versions"
  instance_class         = "db.t3.micro"                            # checked this in aws portal in DB creation, don't know them by heart
  multi_az               = false                                    # Don't want availability zones, but still have to define two subnets in different AZs in the subnet group.
  db_name                = "pcd_db"                                 # project cloud devops DB, has changed to "db_name" instead of just "name"
  username               = "pcd_pgadmin"                            # project cloud devops PostGres admin
  password               = var.pcd_password                         # takes this from .secrets.tfvars, add it to .gitignore and set permissions to 0400 !!
  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.name # defined in line 39
  availability_zone      = "eu-central-1a"                          # this is basically choosing the subnet
  skip_final_snapshot    = true                                     # if set to false, you also need to provide (via final_snapshot_identifier) the name of a final snapshot that will be taken before you destroy this resource (for example with terraform destroy)
  vpc_security_group_ids = [aws_security_group.rds.id]              # add security group
  # parameter_group_name = "default.postgres16.1"                   # need to read up on this, but disabled since it's optional
}
# The database address for connection
output "db_address" {
  value = aws_db_instance.default.address
}

# The database port for connection
output "db_port" {
  value = aws_db_instance.default.port
}