locals {
  env_name                    = "development"
  function_name               = "text_scrambler"
  function_handler            = "test.handler"
  function_runtime            = "python3.9"
  function_timeout_in_seconds = 5

  function_source_dir = "${path.module}/aws_lambda_functions/${local.function_name}"
  # path.module = root path to the function - e.g. ./.terraform/modules/aws_lambda_function/
  # path.cwd = path to run the command - .
}
############## IAM ###############
resource "aws_iam_role" "function_role" {
  name = "${local.function_name}-${local.env_name}"

  # I am adding this one, providing credentials to access AWS resources
  assume_role_policy = jsonencode({
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

########## Archive FIle Resource #############
data "archive_file" "function_zip" {
  source_dir  = local.function_source_dir          # source directory for function
  type        = "zip"                              # Type of the file is .ZIP
  output_path = "${local.function_source_dir}.zip" # output directory for funxtion
}

resource "aws_iam_role_policy_attachment" "AWSLambdaVPCAccessExecutionRole" {
  role       = aws_iam_role.function_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_lambda_function" "function" {
  function_name = "${local.function_name}-${local.env_name}" # text_scrambler-development
  handler       = local.function_handler                     # "test.handler" - which entrypoint in the code to start the script (.py)
  runtime       = local.function_runtime                     # python3.9
  timeout       = local.function_timeout_in_seconds          # 5 sec

  filename         = "${local.function_source_dir}.zip"                 # .terraform/modules/aws_lambda_functions/text_scrambler.zip
  source_code_hash = data.archive_file.function_zip.output_base64sha256 # hash of .ZIP, hash from archive function

  role = aws_iam_role.function_role.arn # reference for aws_lambda_function

  vpc_config {
    # Every subnet should be able to reach an EFS mount target in the same Availability Zone. Cross-AZ mounts are not permitted.
    subnet_ids         = ["${aws_subnet.db_subnet_1.id}", "${aws_subnet.db_subnet_2.id}"]
    security_group_ids = [aws_security_group.lambda.id]
  }

  environment {
    variables = {
      ENVIRONMENT = local.env_name # development
    }
  }
}
